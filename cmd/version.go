package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
)

var version = "unknown"

var versionCommand = &cobra.Command{
	Use: "version",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(version)
	},
}
