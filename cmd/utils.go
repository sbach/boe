package cmd

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/spf13/cobra"

	"boe/internal/context"
	"boe/internal/manifest"
	"boe/internal/oe"
	"boe/internal/utils"
)

func Context(c *context.Context) error {
	c.Dir = opts.buildDir

	if !opts.envReset {
		// Use the host's environment.
		env, err := utils.Environ()
		if err != nil {
			return err
		}
		c.Env = env
	}

	// Set environment variables declared over the command-line.
	c.Env.ImportSlice(opts.env)

	// Set environment variables matching manifest entries.
	c.Env.Set("DISTRO", c.Manifest.Distro, false)
	c.Env.Set("TEMPLATECONF", c.Manifest.TemplateDir, false)

	// Set environment variables matching command-line arguments.
	c.Env.Set("DISTRO", opts.distro, false)
	c.Env.Set("TEMPLATECONF", opts.templateDir, false)

	// Set build directory to use.
	c.Env.Set("BDIR", opts.buildDir, false)

	// Set BUILDDIR ahead of time to match its definition after the
	// 'oe-init-build-env' script is sourced.
	c.Env.Set("BUILDDIR", opts.buildDir, false)

	return nil
}

func Shell(c *context.Context, args []string) error {
	// Set the machine to use.
	if len(opts.machine) > 0 {
		c.Env.Set("MACHINE", opts.machine[0], false)
	}

	var cmd *utils.Cmd
	if len(args) == 0 {
		cmd = utils.Command(opts.shell, opts.shellArgs...)
	} else {
		cmd = utils.Command(args[0], args[1:]...)
	}

	cmd.Dir = c.Dir
	cmd.Env = &c.Env
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		if err, ok := err.(*exec.ExitError); ok {
			switch err.ExitCode() {
			// Terminated by the user.
			case 130:
				return nil
			default:
				return err
			}
		}

		return err
	}

	return nil
}

func Targets(c *context.Context, targets []string) error {
	for _, name := range targets {
		target, found := c.Manifest.Targets[name]
		if !found {
			return fmt.Errorf("Target %s is not available in the manifest", name)
		}

		machines := utils.Filter(target.Machines, func(item string) bool {
			return (len(opts.machine) == 0 || utils.Contains(opts.machine, item)) && !utils.Contains(opts.machineRemove, item)
		})
		machines = append(machines, opts.machineAdd...)

		if len(machines) == 0 {
			return fmt.Errorf("No machines available for target %s", name)
		}

		if err := oe.BitbakeTarget(c, target, machines); err != nil {
			return fmt.Errorf("Could not execute the target %s: %v", name, err)
		}
	}

	return nil
}

func ValidTarget(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
	cmd.Flags().Parse(args)

	manifest, err := manifest.NewManifest(opts.manifestFile)
	if err != nil {
		return nil, cobra.ShellCompDirectiveNoFileComp
	}

	var targets []string
	for name, target := range manifest.Targets {
		if strings.HasPrefix(name, toComplete) {
			if target.Description != "" {
				name += "\t" + target.Description
			}
			targets = append(targets, name)
		}
	}

	return targets, cobra.ShellCompDirectiveNoFileComp
}
