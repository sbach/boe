package cmd

import (
	"github.com/spf13/cobra"

	"boe/internal/context"
	"boe/internal/manifest"
	"boe/internal/oe"
)

var buildCommand = &cobra.Command{
	Use:     "build",
	Aliases: []string{"b", "bu"},

	Short: "Build the specified targets",
	Long:  ``,

	SilenceUsage: true,

	Args: cobra.MinimumNArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		manifest, err := manifest.NewManifest(opts.manifestFile)
		if err != nil {
			return err
		}

		context := context.New(&manifest)
		if err := Context(context); err != nil {
			return err
		}

		if err := oe.Source(context); err != nil {
			return err
		}

		if err := Targets(context, args); err != nil {
			return err
		}

		return nil
	},

	ValidArgsFunction: ValidTarget,
}
