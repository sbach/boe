package cmd

type Options struct {
	buildDir      string
	debug         bool
	distro        string
	env           []string
	envReset      bool
	machine       []string
	machineAdd    []string
	machineRemove []string
	manifestFile  string
	reuse         bool
	shell         string
	shellArgs     []string
	silent        bool
	templateDir   string
}

var opts = Options{}
