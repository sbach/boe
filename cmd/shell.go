package cmd

import (
	"github.com/spf13/cobra"

	"boe/internal/context"
	"boe/internal/manifest"
	"boe/internal/oe"
)

var shellCommand = &cobra.Command{
	Use:     "shell",
	Aliases: []string{"s", "sh"},

	Short: "Get a preconfigured shell environment",
	Long:  ``,

	SilenceUsage: true,

	RunE: func(cmd *cobra.Command, args []string) error {
		manifest, err := manifest.NewManifest(opts.manifestFile)
		if err != nil {
			return err
		}

		context := context.New(&manifest)
		if err := Context(context); err != nil {
			return err
		}

		if !opts.reuse {
			if err := oe.PreSource(context); err != nil {
				return err
			}
		}

		if err := oe.Source(context); err != nil {
			return err
		}

		if !opts.reuse {
			if err := oe.PostSource(context); err != nil {
				return err
			}
		}

		if err := Shell(context, args); err != nil {
			return err
		}

		return nil
	},
}
