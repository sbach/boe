package cmd

import (
	"github.com/spf13/cobra"

	flag "github.com/spf13/pflag"
)

var rootCommand = &cobra.Command{
	Use: "boe",

	Long: `Manage BitBake-based projects, using declarative configuration.`,

	SilenceUsage: true,
}

func Execute() error {
	bitbakeFlagSet := flag.NewFlagSet("", flag.ContinueOnError)
	bitbakeFlagSet.StringSliceVar(&opts.machineAdd, "add-machine", []string{}, "comma-separated list of target device(s) to also use (MACHINE)")
	bitbakeFlagSet.StringSliceVar(&opts.machineRemove, "remove-machine", []string{}, "comma-separated list of target device(s) to not use (MACHINE)")

	commonFlagSet := flag.NewFlagSet("", flag.ContinueOnError)
	commonFlagSet.BoolVar(&opts.envReset, "env-reset", false, "do not use the host's environment variables")
	commonFlagSet.StringSliceVar(&opts.env, "env", []string{}, "environment(s) variables to import")
	commonFlagSet.StringSliceVar(&opts.shellArgs, "shell-arg", []string{"--norc", "-i"}, "comma-separated list of arguments for the shell command specified")
	commonFlagSet.StringVar(&opts.manifestFile, "manifest", "boe.jsonnet", "manifest file")
	commonFlagSet.StringVar(&opts.shell, "shell", "bash", "shell program to use")

	oeFlagSet := flag.NewFlagSet("", flag.ContinueOnError)
	oeFlagSet.StringVar(&opts.buildDir, "build-dir", "build", "build directory (BDIR, BUILDDIR)")
	oeFlagSet.StringVar(&opts.templateDir, "template-dir", "", "template configuration directory (TEMPLATECONF)")
	oeFlagSet.StringVar(&opts.distro, "distro", "", "distribution short name (DISTRO)")
	oeFlagSet.StringSliceVar(&opts.machine, "machine", []string{}, "comma-separated list of target device(s) to use (MACHINE)")

	shellCommandFlagSet := flag.NewFlagSet("", flag.ContinueOnError)
	shellCommandFlagSet.BoolVar(&opts.reuse, "reuse", false, "reuse the build directory as-is")

	shellCommand.Flags().AddFlagSet(commonFlagSet)
	shellCommand.Flags().AddFlagSet(oeFlagSet)
	shellCommand.Flags().AddFlagSet(shellCommandFlagSet)
	rootCommand.AddCommand(shellCommand)

	initCommand.Flags().AddFlagSet(commonFlagSet)
	initCommand.Flags().AddFlagSet(oeFlagSet)
	rootCommand.AddCommand(initCommand)

	buildCommand.Flags().AddFlagSet(bitbakeFlagSet)
	buildCommand.Flags().AddFlagSet(commonFlagSet)
	buildCommand.Flags().AddFlagSet(oeFlagSet)
	rootCommand.AddCommand(buildCommand)

	targetCommand.Flags().AddFlagSet(bitbakeFlagSet)
	targetCommand.Flags().AddFlagSet(commonFlagSet)
	targetCommand.Flags().AddFlagSet(oeFlagSet)
	rootCommand.AddCommand(targetCommand)

	rootCommand.AddCommand(versionCommand)

	rootCommand.SetHelpCommand(&cobra.Command{Hidden: true})

	return rootCommand.Execute()
}
