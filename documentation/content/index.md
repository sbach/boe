---
title: About
hide:
  - navigation
---

The `boe` tool configures and builds [OpenEmbedded][OpenEmbedded]-based [BSP][BSP]s through a declarative configuration file.

#### Goals

* [ ] Teach the standard processes of configuring and building an [OpenEmbedded][OpenEmbedded]-based [BSP][BSP]s.
* [ ] Cover the needs of new users as well as seasoned developers.
* [x] Configuration of a build can be described in a single file.
* [x] Respect the standard [OpenEmbedded][OpenEmbedded] practices.
* [x] Minimize host dependencies.

#### Features

* Command-line interface with auto-completion features.
* Use [`jsonnet`][jsonnet] as configuration language.

## Getting started

`boe` is built and hosted on GitLab. No dependencies are required.

=== "From your browser"

    [Download latest version]({{ config.extras.gitlab_api_v4_url }}/projects/{{ config.extras.gitlab_project_id }}/packages/generic/boe/{{ config.extras.git_branch }}/boe){ .md-button .md-button--primary }

=== "From the command line"

    You can retrieve the latest version with:

    ``` shell
    curl --output boe {{ config.extras.gitlab_api_v4_url }}/projects/{{ config.extras.gitlab_project_id }}/packages/generic/boe/{{ config.extras.git_branch }}/boe
    chmod +x ./boe
    ```

*[BSP]: Board Support Package

[BSP]: https://en.wikipedia.org/wiki/Board_support_package
[OpenEmbedded]: https://www.openembedded.org/wiki/Main_Page
[jsonnet]: https://jsonnet.org/
