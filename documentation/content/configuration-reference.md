---
title: Configuration reference
hide:
  - navigation
---

## Keywords

A `boe` project configuration includes the following keywords:

| Keyword                         | Description  |
| ------------------------------- | ------------ |
| [`distro`](#distro)             | Specifies the distribution to use ([`DISTRO`][DISTRO]). |
| [`files`](#files)               | Defines files to manage. |
| [`layers`](#layers)             | Allows to control layers defined in [`conf/bblayers.conf`][bblayers.conf]. |
| [`targets`](#targets)           | Defines targets building [bitbake][bitbake] recipes for the given machines. |
| [`template_dir`](#template_dir) | Specifies the template configuration directory ([`TEMPLATECONF`][TEMPLATECONF]) to use. |

### `distro`

Use the `distro` keyword to set the distribution ([`DISTRO`][DISTRO]) to use when building the [BSP][BSP].

### `files`

Use the `files` keyword to manage files of your [BSP][BSP].

Note that the user is not limited to managing just the standard configuration files (e.g.: `local.conf`).

!!! example "Example of `files` declaration"
    ``` jsonnet
    files: [
      {
        path: '${BUILDDIR}/conf/auto.conf',
        append: true,
        content: |||
          INHERIT += "buildhistory"
          BUILDHISTORY_COMMIT = "1"
        |||
      },
      {
        path: '${HOME}/.gitconfig',
        append: true,
        keep: true,
        content: |||
            […]
        |||
      },
    ]
    ```

    We ask, here above, to add a single optional layer located in `${BUILDDIR}/workspace`, which points to the temporary layer usually created by the [`devtool`][devtool] utility.

#### `files:append`

Use the `append` keyword to append the [`files:content`](#filescontent) to the file.

#### `files:content`

Use the `content` keyword to specify the content of the file.

Based on the value of [`files:append`](#filesappend), the file content is either replaced or appended.

#### `files:keep`

Use the `keep` keyword to keep the file as-is if it already exists.

#### `files:path`

Use the `path` keyword to specify the location of the file to manage.

Note that environment variables are supported, e.g.:
``` jsonnet
  {
    path: '${BUILDDIR}/conf/local.conf',
    […]
  }
```
… will expand `BUILDIR`, which corresponds to the current build directory.






### `layers`

Use the `layers` keyword to manage the layers of your [BSP][BSP].

We leverage the [`bitbake-layers`][bitbake-layers] to add or remove layers in the [`conf/bblayers.conf`][bblayers.conf] configuration file.

!!! example "Example of `layers` declaration"
    ```
    layers: [
      {
        path: '${BUILDDIR}/workspace',
        optional: true,
        remove: false,
      }
    ]
    ```

    We ask, here above, to add a single optional layer located in `${BUILDDIR}/workspace`, which points to the temporary layer usually created by the [`devtool`][devtool] utility.

#### `layers:optional`

Use the `optional` keyword not to abort in case the [`bitbake-layers`][bitbake-layers] does not complete successfully.

#### `layers:path`

Use the `path` keyword to specify the location of the layer to manage.

Note that environment variables are supported, e.g.:
``` jsonnet
  {
    path: '${BUILDDIR}/workspace',
    […]
  }
```
… will expand `BUILDIR`, which corresponds to the current build directory.

#### `layers:remove`

Use the `remove` keyword to request the layer to be removed from [`conf/bblayers.conf`][bblayers.conf].

Note that by default (`false`), the underlying command executed is `bitbake-layers add-layer`.

### `targets`

Use the `targets` keyword to define `boe` targets consisting of recipes to [`bitbake`][bitbake] for a set of machines.

Note that at least one target must be defined for proper usage of the `build` and `target` sub-commands of the `boe` utility.

!!! example "Example of `targets` declaration"
    ```
    targets: {
      'kernel-clean': {
        description: 'Build the Linux kernel recipe for qemuarm64 without cache.',
        bitbake: [
          ['linux-kernel', '-c', 'cleanall'],
          ['linux-kernel'],
        ],
        machines: ['qemuarm64'],
      }
    }
    ```
    We defined, here above, a single target named `kernel-clean` along with its description and two [`bitbake`][bitbake] commands to execute for the `qemuarm64` machine.

    Usage: `boe [build|target] kernel-clean`.

#### `targets:bitbake`

Use the `bitbake` keyword to define a list of [`bitbake`][bitbake] commands to execute for each machine specified in [`targets:machines`](#targetsmachines).

For example, the below declaration:
``` jsonnet
  bitbake: [
    ['linux-kernel', '-c', 'cleanall'],
    ['linux-kernel'],
  ]
```
… is equivalent to the shell commands below:
``` jsonnet
$ bitbake linux-kernel -c cleanall
$ bitbake linux-kernel
```

#### `targets:description`

Use the `description` keyword to provide a description to this target, visible during auto-completion.

For example, considering the following target declaration:
``` jsonnet
'package-index': {
  description: 'Build the index for the package manager',
  […]
}
```
…and provided the user sourced the completion script for `boe`, using ++tab++ to auto-complete the below command will yield:
``` shell
$ boe target
package-index  (Build the index for the package manager)
[…]
```

#### `targets:machines`

Use `targets:machines` to specify the list of [`MACHINE`][MACHINE] to execute the [`targets:bitbake`](#targetsbitbake) entries for.

For example, the following target declaration:
``` jsonnet
'kernel': {
  […]
  bitbake: [['linux-kernel']],
  machines: ['qemuarm64', 'qemuarm'],
}
```
… is equivalent, by default, to the shell commands below:
``` shell
$ export MACHINE=qemuarm64
$ bitbake linux-kernel
$ export MACHINE=qemuarm
$ bitbake linux-kernel
```

### `template_dir`

Use the `template_dir` keyword to set the directory ([`TEMPLATECONF`][TEMPLATECONF]) to find templates from which to build the configuration files.

## Built-in variables and functions

The `boe` utility embeds Jsonnet libraries that can be [imported](https://jsonnet.org/learning/tutorial.html#imports) with `import 'boe::[…]'`.

### The `bb` library

| Type | Usage example | Description |
| ---- | ------| ----------- |
| Function | `#!jsonnet env('HOME')` | Retrieves the value of an environment variable. |
| Function | `#!jsonnet script(multi-line-string)` | Executes the shell script at the provided path and returns its standard output. |
| Helper | `#!jsonnet layer.add('meta-test')` | Helper function to define a layer to add.<br>Optional argument: `#!jsonnet optional=[true|false]` |
| Helper | `#!jsonnet layer.remove('meta-test')` | Helper function to define a layer to remove. |
| Helper | `#!jsonnet files.auto_conf` | Helper variable defining a file to manage pointing to [`conf/auto.conf`][auto.conf]. |
| Helper | `#!jsonnet files.bblayers_conf` | Helper variable defining a file to manage pointing to [`conf/bblayers.conf`][bblayers.conf]. |
| Helper | `#!jsonnet files.local_conf` | Helper variable defining a file to manage pointing to [`conf/local.conf`][local.conf]. |
| Helper | `#!jsonnet files.site_conf` | Helper variable defining a file to manage pointing to [`conf/site.conf`][site.conf]. |

??? example "Library usage example in `boe.jsonnet`"
    ``` jsonnet title="Snippet of boe.jsonnet" linenums="1"
    local bb = import 'boe::bb';

    {
      layers: [
        bb.layer.add('${BUILDDIR}/workspace', optional=true),
        bb.layer.add('meta-clang'),
        bb.layer.remove('meta-rpi'),
      ],

      files: [
        bb.files['site_conf'] {
          content: |||
            LUKS_PASSPHRASE = "%(key)s"
          ||| % {
            key: bb.script(|||
              #!/bin/sh
              openssl rand -base64 48 | base32
              |||),
          },
        }
      ]
    }
    ```

## Reference configuration file

``` jsonnet title="boe.jsonnet" linenums="1"
# Import the boe JSONNET library.
local bb = import 'boe::bb';

# Define variable containing the list of machines to build for.
local machines = ['qemuarm64'];

# Beginning of JSON document.
{
  # Define layers to add or remove to 'bblayers.conf'.
  layers: [
    bb.layer.add('${BUILDDIR}/workspace', optional=true),
  ],

  # Use the specified directory as 'TEMPLATECONF' definition.
  template_dir: 'meta/conf/templates/default',

  # Manage and optionally append to configuration files.
  files: [
    # Append to 'conf/local.conf' to enable the build history.
    bb.files['local_conf'] {
      content: |||
        INHERIT += "buildhistory"
        BUILDHISTORY_COMMIT = "1"
      |||
    }
  ],

  # Use the specified distribution as 'DISTRO' definition.
  distro: 'defaultsetup',

  # Define a list of buildable targets.
  targets: {
    'all': {
      description: 'Build common recipes',
      bitbake: [
        ['core-image-minimal'],
        ['meta-toolchain'],
      ],
      machines: machines,
    },

    'package-index': {
      description: 'Build the index for the package manager',
      bitbake: [
        ['package-index']
      ],
      machines: machines,
    },
  },
}
```


*[BSP]: Board Support Package

[BSP]: https://en.wikipedia.org/wiki/Board_support_package
[bitbake-layers]: https:// ""
[devtool]: https:// ""

[DISTRO]: https://docs.yoctoproject.org/ref-manual/variables.html#term-DISTRO "Read about this variable, used to select the distribution configuration file, in the official variables glossary."
[MACHINE]: https://docs.yoctoproject.org/ref-manual/variables.html#term-MACHINE "Read about this variable, used to select the target device for which recipes are built, in the official variables glossary."
[TEMPLATECONF]: https://docs.yoctoproject.org/ref-manual/variables.html#term-TEMPLATECONF "Read about this variable, used to specify the directory where to find templates, in the official variables glossary."

[auto.conf]: https://docs.yoctoproject.org/dev/overview-manual/concepts.html#user-configuration "Read about this configuration file, in the official manual."
[bblayers.conf]: https://docs.yoctoproject.org/ref-manual/structure.html#build-conf-bblayers-conf "Read about the layers configuration file, in the official variables glossary."
[local.conf]: https://docs.yoctoproject.org/ref-manual/structure.html#build-conf-local-conf "Read about the local configuration file, in the official variables glossary."
[site.conf]: https://docs.yoctoproject.org/dev/overview-manual/concepts.html#user-configuration "Read about the site configuration file, in the official documentation."

[bitbake]: https://docs.yoctoproject.org/bitbake/2.4/bitbake-user-manual/bitbake-user-manual-intro.html#the-bitbake-command "Read about the bitbake command in the official documentation"
