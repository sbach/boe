---
title: Command line
hide:
  - navigation
---

## Command line usage

The `boe` utility provides the following commands:

| Name                | Description | Use case |
| ------------------- | ----------- | -------- |
| [`init`](#init)     | Initialize the build environment. | Simplifies the creation of the build directory and its configuration files. |
| [`build`](#build)   | Build the specified targets. | Simplifies the execution of multiple [`bitbake`][bitbake] commands for multiple machines. |
| [`shell`](#shell)   | Get a preconfigured shell environment. | Get the developer right into a preconfigured shell for [`bitbake`][bitbake] usage. |
| [`target`](#target) | Initialize and build the specified targets.<br>Combines the [`init`](#init) and [`build`](#build) commands in one. | Setup and compile in a reproducible manner the [BSP][BSP] as described.

### Command line options

| Name             | Applicable to `boe…`            | Default value | Description |
| ---------------- | ------------------------------- | ------------- | ----------- |
| `manifest`       | `init`,`build`,`shell`,`target` | `boe.jsonnet` | Manifest file |
| `shell`          | `init`,`build`,`shell`,`target` | `bash`        | Shell program to use |
| `shell-arg`      | `init`,`build`,`shell`,`target` | `--norc -i`   | Comma-separated list of arguments for the shell command specified |
| `env`            | `init`,`build`,`shell`,`target` |               | Environment(s) variables to import |
| `env-reset`      | `init`,`build`,`shell`,`target` | `false`       | Do not use the host's environment variables |
| `build-dir`      | `init`,`build`,`shell`,`target` | `build`       | Build directory (`BDIR`, `BUILDDIR`) |
| `template-dir`   | `init`,`shell`,`target`         |               | Template configuration directory ([`TEMPLATECONF`][TEMPLATECONF]) |
| `distro`         | `init`,`build`,`shell`,`target` |               | Distribution short name ([`DISTRO`][DISTRO]) |
| `machine`        | `init`,`build`,`shell`,`target` |               | Comma-separated list of target device(s) ([`MACHINE`][MACHINE]) to use |
| `add-machine`    | `build`,`target`                |               | Comma-separated list of target device(s) ([`MACHINE`][MACHINE]) to also use |
| `remove-machine` | `build`,`target`                |               | Comma-separated list of target device(s) ([`MACHINE`][MACHINE]) to not use |
| `reuse`          | `shell`                         | `false`       | Reuse the build directory as-is |

### :material-console: `boe init`

The `init` command will initialize the build directory while respecting standard practices:

1. Export selected configuration file template directory through [`TEMPLATECONF`][TEMPLATECONF].
2. Cleanup any existing configuration files managed.
3. Source the environment initialization script.
4. Edit the configuration files in the `conf` folder of the build directory.
5. Configure layers using [`bitbake-layers`][bitbake-layers].

Note that based on the directives in your `boe.jsonnet`, the files managed may be-recreated.

### :material-console: `boe build`

The `build` command will execute the [`bitbake`][bitbake] commands, for each machine chosen, in the selected targets. It assumes the build directory is already configured.

### :material-console: `boe shell`

The `shell` command will drop the user into a preconfigured shell environment where the [`bitbake`][bitbake] command is available and the various variables, such as [`DISTRO`][DISTRO] and optionally [`MACHINE`][MACHINE], are already exported.

!!! warning "This command will re-initialize the build configuration files (see [`boe init`](#boeinit)) by default."

    This behavior can be changed using the `--reuse` flag.

### :material-console: `boe target`

The `target` command combines the [`boe init`](#boeinit) and [`boe build`](#boebuild) commands. It is meant to quickly build the [BSP][BSP] exactly as described in the manifest file.

*[BSP]: Board Support Package

[BSP]: https://en.wikipedia.org/wiki/Board_support_package
[DISTRO]: https://docs.yoctoproject.org/ref-manual/variables.html#term-DISTRO "Read about this variable, used to select the distribution configuration file, in the official variables glossary."
[MACHINE]: https://docs.yoctoproject.org/ref-manual/variables.html#term-MACHINE "Read about this variable, used to select the target device for which recipes are built, in the official variables glossary."
[TEMPLATECONF]: https://docs.yoctoproject.org/ref-manual/variables.html#term-TEMPLATECONF "Read about this variable, used to specify the directory where to find templates, in the official variables glossary."
[bitbake-layers]: https:// ""
[bitbake]: https://docs.yoctoproject.org/bitbake/2.4/bitbake-user-manual/bitbake-user-manual-intro.html#the-bitbake-command "Read about the bitbake command in the official documentation"
