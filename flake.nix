{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.systems.url = "github:nix-systems/default";

  outputs = { self, nixpkgs, systems }: let
    supportedSystems = nixpkgs.lib.genAttrs (import systems);
    forEachSystem = function: supportedSystems (system:
        function nixpkgs.legacyPackages.${system});
  in {
   devShells = forEachSystem (pkgs: {
      default = pkgs.mkShell {
        packages = with pkgs; [
          git
          gnumake
          go
        ];
      };

      documentation = pkgs.mkShell {
        buildInputs = with pkgs; [
          mkdocs
          python3.pkgs.mkdocs-git-revision-date-localized-plugin
          python3.pkgs.mkdocs-macros
          python3.pkgs.mkdocs-material
          python3.pkgs.setuptools
        ];
        shellHook = ''
          exec mkdocs serve
        '';
      };
    });
  };
}
