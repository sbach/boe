GO ?= go
RM ?= rm
GOFLAGS ?=
PREFIX ?= /usr/local
BINDIR ?= bin

VERSION ?= $(shell git describe --always --dirty --tags)

go_ldflags := -X 'boe/cmd.version=$(VERSION)'
go_flags := $(GOFLAGS) -ldflags="$(go_ldflags)"

all: boe

boe:
	CGO_ENABLED=0 $(GO) build $(go_flags)

clean:
	$(RM) -f boe

install:
	mkdir -p $(DESTDIR)$(PREFIX)/$(BINDIR)
	cp -f boe $(DESTDIR)$(PREFIX)/$(BINDIR)

.PHONY: boe clean install
