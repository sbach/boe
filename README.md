## **B**uild **O**pen**E**mbedded utility
A tool to configure and build [OpenEmbedded](https://www.openembedded.org/wiki/Main_Page)-based BSPs[^1] using declarative configuration.\
[**Explore the docs »**](https://sbach.gitlab.io/boe/)

### How to contribute to the documentation

We are using [Material](https://squidfunk.github.io/mkdocs-material) for [MkDocs](https://www.mkdocs.org) to render our documentation. You can contribute by locally editing the Markdown files in [`documentation/content/`](documentation/content/) or directly, from the documentation pages, by using the `Edit this page` option at the top-right.

Please refer to MkDocs [user guide](https://www.mkdocs.org/user-guide/writing-your-docs/) as well as the Material for MkDocs [reference](https://squidfunk.github.io/mkdocs-material/reference/).

Note that you can preview your changes using `mkdocs serve`.\
You may also use the provided container image:
```
docker build --tag mkdocs documentation
docker run --net=host --rm --volume ${PWD}:/data --workdir /data mkdocs
```

[^1]: [Board Support Packages](https://en.wikipedia.org/wiki/Board_support_package)
