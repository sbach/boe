package context

import (
	"boe/internal/manifest"
	"boe/internal/utils"
)

type Context struct {
	Dir      string
	Env      utils.Environment
	Manifest *manifest.Manifest
}
