package context

import (
	"boe/internal/manifest"
)

func New(m *manifest.Manifest) *Context {
	return &Context{
		Env:      map[string]string{},
		Manifest: m,
	}
}
