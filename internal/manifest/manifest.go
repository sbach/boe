package manifest

import (
	"encoding/json"

	gjst "github.com/google/go-jsonnet"

	bjst "boe/internal/jsonnet"
)

func NewManifest(filepath string) (manifest Manifest, err error) {
	vm := gjst.MakeVM()

	for _, fn := range bjst.NativeFunctions {
		vm.NativeFunction(fn)
	}

	vm.Importer(bjst.NewCustomImporter(nil, bjst.EmbeddedLibraries))

	parsed, err := vm.EvaluateFile(filepath)
	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(parsed), &manifest)

	return
}
