package manifest

type Manifest struct {
	Distro      string            `json:"distro"`
	Files       []File            `json:"files"`
	Layers      []Layer           `json:"layers"`
	Targets     map[string]Target `json:"targets"`
	TemplateDir string            `json:"template_dir"`
}

type File struct {
	Append  bool   `json:"append"`
	Content string `json:"content"`
	Keep    bool   `json:"keep"`
	Path    string `json:"path"`
}

type Layer struct {
	Optional bool   `json:"optional"`
	Path     string `json:"path"`
	Remove   bool   `json:"remove"`
}

type Target struct {
	Bitbake     [][]string `json:"bitbake"`
	Description string     `json:"description"`
	Machines    []string   `json:"machines"`
}
