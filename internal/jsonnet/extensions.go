package jsonnet

import (
	"embed"
	"fmt"
	"os"
	"strings"

	"github.com/google/go-jsonnet"
	"github.com/google/go-jsonnet/ast"

	"boe/internal/utils"
)

//go:embed *.libsonnet
var EmbeddedLibraries embed.FS

var NativeFunctions = []*jsonnet.NativeFunction{
	{
		Name:   "env",
		Params: ast.Identifiers{"name"},
		Func: func(args []interface{}) (interface{}, error) {
			name, ok := args[0].(string)
			if !ok {
				return nil, fmt.Errorf("unexpected type %T for 'name' argument", args[0])
			}
			value, ok := os.LookupEnv(name)
			if !ok {
				return nil, fmt.Errorf("environment variable: %s, is not set", name)
			}
			return value, nil
		},
	},
	{
		Name:   "script",
		Params: ast.Identifiers{"script"},
		Func: func(args []interface{}) (interface{}, error) {
			script, ok := args[0].(string)
			if !ok {
				return nil, fmt.Errorf("unexpected type %T for 'script' argument", args[1])
			}

			cmd, file, err := utils.CommandFile([]byte(script))
			if err != nil {
				return nil, err
			}
			defer file.Close()

			stdout := &strings.Builder{}
			cmd.Stdout = stdout

			if err := cmd.Run(); err != nil {
				return nil, err
			}

			if output := stdout.String(); output != "" {
				return output, nil
			} else {
				return nil, fmt.Errorf("empty standard output; script: %s", script)
			}
		},
	},
}
