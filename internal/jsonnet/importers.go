package jsonnet

import (
	"embed"
	"strings"

	"github.com/google/go-jsonnet"
)

type EmbedFSImporter struct {
	fs embed.FS
}

func (e *EmbedFSImporter) Import(importedFrom, importedPath string) (contents jsonnet.Contents, foundAt string, err error) {
	content, err := e.fs.ReadFile(importedPath)
	if err != nil {
		return
	}

	return jsonnet.MakeContentsRaw(content), importedPath, nil
}

type CustomImporter struct {
	eImporter EmbedFSImporter
	fImporter jsonnet.FileImporter
}

func (c *CustomImporter) Import(importedFrom, importedPath string) (contents jsonnet.Contents, foundAt string, err error) {
	if path, found := strings.CutPrefix(importedPath, EmbeddedLibrariesPrefix); found {
		return c.eImporter.Import(importedFrom, path+LibsonnetFileExtension)
	} else {
		return c.fImporter.Import(importedFrom, importedPath)
	}
}

func NewCustomImporter(paths []string, fs embed.FS) *CustomImporter {
	return &CustomImporter{
		fImporter: jsonnet.FileImporter{JPaths: paths},
		eImporter: EmbedFSImporter{fs: fs},
	}
}
