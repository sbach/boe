{
  env(name):: std.native('env')(name),
  script(script):: std.native('script')(script),

  files: {
    'auto.conf': {
      path: '${BUILDDIR}/conf/auto.conf',
      append: true,
    },
    'bblayers.conf': {
      path: '${BUILDDIR}/conf/bblayers.conf',
      append: true,
    },
    'local.conf': {
      path: '${BUILDDIR}/conf/local.conf',
      append: true,
    },
    'site.conf': {
      path: '${BUILDDIR}/conf/site.conf',
      append: true,
    },
  },

  layer: {
    add(path, optional=false)::
      {
        path: path,
        optional: optional,
        remove: false,
      },

    remove(path, optional=false)::
      {
        path: path,
        optional: optional,
        remove: true,
      },
  },
}
