package oe

import (
	"boe/internal/context"
	"boe/internal/manifest"
)

func PreSource(c *context.Context) error {
	// Remove existing configuration files listed in the manifest.
	for _, file := range c.Manifest.Files {
		if err := RemoveFile(c, file); err != nil {
			return err
		}
	}

	return nil
}

func PostSource(c *context.Context) error {
	// Create the configuration files listed in the manifest.
	for _, file := range c.Manifest.Files {
		if err := ConfigureFile(c, file); err != nil {
			return err
		}
	}

	// Handle the layers listed in the manifest.
	for _, layer := range c.Manifest.Layers {
		if err := ConfigureLayer(c, layer); err != nil {
			return err
		}
	}

	return nil
}

func BitbakeTarget(ctx *context.Context, target manifest.Target, machines []string) error {
	for _, machine := range machines {
		// Set the machine to use.
		ctx.Env.Set("MACHINE", machine, false)

		// Execute, sequentially, the bitbake calls listed in the manifest's target.
		for _, args := range target.Bitbake {
			if err := Bitbake(ctx, args); err != nil {
				return err
			}
		}
	}

	return nil
}
