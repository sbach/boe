package oe

import (
	"os"

	"boe/internal/context"
	"boe/internal/manifest"
	"boe/internal/utils"
)

var CommandBitbake = "bitbake"
var CommandAddLayer = "bitbake-layers add-layer"
var CommandRemoveLayer = "bitbake-layers remove-layer"
var ScriptSourceOEEnv = `#!/bin/sh
. ./oe-init-build-env
env > ${ENV_FILE}
`

// TODO: Need to print what's in stderr in case of failure so the user understands
// Mean to test: set a non existant TEMPLATECONF directory.
func Source(ctx *context.Context) error {
	cmd, file, err := utils.CommandFile([]byte(ScriptSourceOEEnv))
	if err != nil {
		return err
	}
	defer file.Close()

	cmd.Env = &ctx.Env
	cmd.Stderr = os.Stderr

	envClosure, err := cmd.AddEnvironmentHook("ENV_FILE")
	if err != nil {
		return err
	}

	err = cmd.Run()

	if err := envClosure(); err != nil {
		return err
	}

	return err
}

func Bitbake(ctx *context.Context, args []string) error {
	cmd := utils.Command(CommandBitbake, args...)
	cmd.Dir = ctx.Dir
	cmd.Env = &ctx.Env
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

func ConfigureFile(ctx *context.Context, file manifest.File) error {
	if file.Keep {
		return nil
	}

	path := ctx.Env.Expand(file.Path)

	var flag int
	if file.Append {
		flag = os.O_APPEND
	} else {
		flag = os.O_TRUNC
	}

	return utils.FileWriteString(path, file.Content, flag)
}

func RemoveFile(ctx *context.Context, file manifest.File) error {
	if file.Keep {
		return nil
	}

	path := ctx.Env.Expand(file.Path)

	if err := os.Remove(path); !os.IsNotExist(err) {
		return err
	}

	return nil
}

func ConfigureLayer(ctx *context.Context, layer manifest.Layer) error {
	command := CommandAddLayer
	if layer.Remove {
		command = CommandRemoveLayer
	}

	path := ctx.Env.Expand(layer.Path)

	cmd := utils.Command(command, path)
	cmd.Dir = ctx.Dir
	cmd.Env = &ctx.Env

	if err := cmd.Run(); err != nil && !layer.Optional {
		return err
	}

	return nil
}
