package utils

import (
	"fmt"
	"os"
	"path/filepath"
)

func Contains[T comparable](items []T, item T) bool {
	for _, v := range items {
		if v == item {
			return true
		}
	}
	return false
}

func Filter[T any](items []T, filterFunc func(T) bool) (ret []T) {
	for _, item := range items {
		if filterFunc(item) {
			ret = append(ret, item)
		}
	}
	return
}

func Which(name string, path string) (string, error) {
	if filepath.IsAbs(name) {
		return name, nil
	}

	for _, dir := range filepath.SplitList(path) {
		path := filepath.Join(dir, name)
		if info, err := os.Stat(path); err == nil && isExecutable(info) {
			return path, nil
		}
	}

	return "", fmt.Errorf("")
}
