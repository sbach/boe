package utils

import (
	"fmt"
	"os"

	"golang.org/x/sys/unix"
)

func FileMemfdCreate() (*os.File, error) {
	fd, err := unix.MemfdCreate("", unix.MFD_CLOEXEC)
	if err != nil {
		return nil, err
	}

	fn := fmt.Sprintf("/proc/%d/fd/%d", os.Getpid(), fd)
	return os.NewFile(uintptr(fd), fn), nil
}

func FileWriteString(filepath string, text string, flag int) error {
	file, err := os.OpenFile(filepath, flag|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	if _, err := file.WriteString(text); err != nil {
		return err
	}

	return file.Sync()
}
