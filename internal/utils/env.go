package utils

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var EnvironmentKeyValueSeparator = "="

type Environment map[string]string

func (e Environment) Expand(s string) string {
	return os.Expand(s, e.Get)
}

func (e Environment) Get(key string) string {
	value, _ := e[key]
	return value
}

func (e Environment) ImportFile(file *os.File) error {
	sc := bufio.NewScanner(file)
	for sc.Scan() {
		entry := sc.Text()
		kv := strings.SplitN(entry, EnvironmentKeyValueSeparator, 2)
		if len(kv) != 2 {
			return fmt.Errorf("invalid entry: %s", entry)
		}
		e[kv[0]] = kv[1]
	}
	return nil
}

func (e Environment) ImportSlice(entries []string) {
	for _, entry := range entries {
		entry := strings.SplitN(entry, EnvironmentKeyValueSeparator, 2)
		switch len(entry) {
		case 1:
			if value := os.Getenv(entry[0]); value != "" {
				e[entry[0]] = value
			}
		case 2:
			e[entry[0]] = entry[1]
		}
	}
}

func (e Environment) Set(key string, value string, allowEmpty bool) {
	if allowEmpty || value != "" {
		e[key] = value
	}
}

func (e Environment) ToSlice() []string {
	s := make([]string, len(e))
	index := 0
	for key, value := range e {
		s[index] = key + EnvironmentKeyValueSeparator + value
		index++
	}
	return s
}

func Environ() (Environment, error) {
	env := make(Environment)
	for _, entry := range os.Environ() {
		kv := strings.SplitN(entry, EnvironmentKeyValueSeparator, 2)
		if len(kv) != 2 {
			return nil, fmt.Errorf("invalid entry: %s", entry)
		}
		env[kv[0]] = kv[1]
	}
	return env, nil
}
