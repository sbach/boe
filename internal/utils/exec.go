package utils

import (
	"fmt"
	"io"
	"os"
	"os/exec"
)

type Cmd struct {
	Name   string
	Args   []string
	Dir    string
	Env    *Environment
	Stderr io.Writer
	Stdin  io.Reader
	Stdout io.Writer
}

func (c *Cmd) Run() (err error) {

	name := c.Name
	if c.Env != nil {
		name, err = Which(c.Name, c.Env.Get("PATH"))
		if err != nil {
			return err
		}
	}

	command := exec.Command(name, c.Args...)
	if err = command.Err; err != nil {
		return err
	}

	command.Dir = c.Dir
	command.Stdin = c.Stdin
	command.Stdout = c.Stdout
	command.Stderr = c.Stderr

	if c.Env != nil {
		command.Env = c.Env.ToSlice()
	}

	return command.Run()
}

func (c *Cmd) AddEnvironmentHook(name string) (func() error, error) {

	if c.Env == nil {
		return nil, fmt.Errorf("")
	}

	file, err := FileMemfdCreate()
	if err != nil {
		return nil, err
	}

	c.Env.Set(name, file.Name(), false)

	return func() error {
		if err := file.Sync(); err != nil {
			return err
		}
		defer file.Close()
		return c.Env.ImportFile(file)
	}, nil
}

func Command(name string, args ...string) *Cmd {
	return &Cmd{
		Name: name,
		Args: args,
	}
}

func CommandFile(data []byte) (*Cmd, *os.File, error) {

	file, err := FileMemfdCreate()
	if err != nil {
		return nil, nil, err
	}

	if _, err := file.Write(data); err != nil {
		file.Close()
		return nil, nil, err
	}

	if err := file.Sync(); err != nil {
		file.Close()
		return nil, nil, err
	}

	return &Cmd{Name: file.Name()}, file, nil
}
