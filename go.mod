module boe

go 1.21

require (
	github.com/google/go-jsonnet v0.20.0
	github.com/spf13/cobra v1.8.0
	github.com/spf13/pflag v1.0.5
	golang.org/x/sys v0.17.0
)

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
	sigs.k8s.io/yaml v1.1.0 // indirect
)
